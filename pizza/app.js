const express = require('express')
const app = express()
app.use(express.static(__dirname+'/public'));
app.set('view engine', 'hbs');

app.get('/', function (req, res) {
  res.render('home');
});

app.listen(3000,()=>{
    console.log("your express app is running in th port 3000");
});
