var express = require('express');
var app = express();
var hbs = require('hbs');

app.use(express.static(__dirname+'/public'));
app.set('view engine', 'hbs');

hbs.registerPartials(__dirname + '/views/partials', function (err) {});

app.get('/',(req,res)=>{
    res.render('home')
})

app.listen(3000,()=>{
    console.log('su servidor de express esta escuchando en el puerto 3000');
})