const express = require('express')
const app = express()
const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');
const url = 'mongodb://mongo:27017';
const dbName = 'manolito'; 

MongoClient.connect(url, function(err, client) {
  assert.equal(null, err);
  console.log("Connected successfully to server"); 
  const db = client.db(dbName); 
  client.close();
});
 
app.get('/', function (req, res) {
  res.send('Hello World')
}) 

app.get('/lista', function (req, res) {
  res.send('esa es uns lñista')
}) 

app.listen(3000, function(){
    console.log("su app express esta escuchando en el puerto 3000")
})